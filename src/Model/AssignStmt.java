package Model;

import java.io.IOException;

public class AssignStmt implements IStmt {
    private String id;
    private Exp exp;

    public AssignStmt(String _id,Exp _exp){this.id=_id;this.exp=_exp;}

    public String toString(){
        return this.id+"="+this.exp.toString();
    }

    public PrgState execute(PrgState state) throws IOException {
        MyIDictionary<String,Integer> symTbl=state.getSymTable();
        try {
            int val = this.exp.eval(symTbl,state.getHeap());
            if (symTbl.isDefined(this.id))
                symTbl.update(this.id, val);
            else
                symTbl.add(this.id, val);
        }catch (MyException exception){
            System.out.println(exception.getMessage());
        }
        return null;
    }
}
