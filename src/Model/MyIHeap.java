package Model;

import java.util.HashMap;
import java.util.Map;

public interface MyIHeap<A,C> {
    int getFreeLocation();
    void incrementFreeLocation();
    void add(A a,C c);
    void remove(A a);
    int lookup(A a);
    String toString();
    boolean isKey(A key);
    void setContent(Map<Integer,Integer> map);
    HashMap<A,C> getContent();
}
