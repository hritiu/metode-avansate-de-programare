package Model;

import java.io.BufferedReader;
import java.io.IOException;

public class readFileStmt implements IStmt {
    private Exp exp_file_id;
    private String var_name;

    public readFileStmt(Exp e,String n){
        this.exp_file_id=e;
        this.var_name=n;
    }

    @Override
    public PrgState execute(PrgState state)throws MyException,IOException{
        MyIDictionary<String,Integer> dictionary=state.getSymTable();
        int val=this.exp_file_id.eval(dictionary,state.getHeap());

        try {
            BufferedReader br = state.getFileTable().lookup(val).getB();

            if(br==null)
                throw new MyException("ERROR: There is no BufferedReader !!!\n");

            int key;
            String str=br.readLine();
            if(str!=null)
                key=Integer.parseInt(str);
            else
                key=0;

            if(!dictionary.isDefined(var_name))
                dictionary.add(var_name,key);
            else
                dictionary.update(var_name,key);

        }catch (NullPointerException npe){
            System.out.println("ERROR: Cannot get BufferedReader => NullPointerException !!!\n");
        }


        return null;
    }
    @Override
    public String toString(){return "readFile("+this.exp_file_id.toString()+","+this.var_name.toString()+")";}
}
