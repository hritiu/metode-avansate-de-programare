package Model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MyHeap implements MyIHeap<Integer,Integer> {
    private HashMap<Integer,Integer> heap;
    private int freeLocation;

    public MyHeap(){
        this.freeLocation=0;
        this.heap=new HashMap<>();
    }

    @Override
    public int getFreeLocation(){
        return this.freeLocation;
    }

    @Override
    public void incrementFreeLocation(){
        this.freeLocation++;
    }

    @Override
    public void add(Integer address,Integer content){
        this.heap.put(address,content);
    }

    @Override
    public void remove(Integer address){
        this.heap.remove(address);
    }

    @Override
    public int lookup(Integer address){
        return this.heap.get(address);
    }

    @Override
    public String toString(){
        return this.heap.toString();
    }

    @Override
    public boolean isKey(Integer key){
        if(this.heap.containsKey(key))
            return true;
        return false;
    }

    @Override
    public void setContent(Map<Integer,Integer> map){
        //for(HashMap.Entry<Integer,Integer> e:this.heap.entrySet())
          //  add(e.getKey(),map.get(e.getKey()));

        heap=new HashMap<>();
        for(Integer i:map.keySet())
            heap.put(i,map.get(i));
    }

    @Override
    public HashMap<Integer,Integer> getContent(){
        return this.heap;
        //HashMap<Integer,Integer> map=new HashMap<>();
       // map=this.heap;
     //   return map;
    }
}
