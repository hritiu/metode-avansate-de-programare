package Model;

public class wHStmt implements IStmt {
    private String variable;
    private Exp expression;

    public wHStmt(String var,Exp e){
        this.expression=e;
        this.variable=var;
    }

    @Override
    public PrgState execute(PrgState state){
        int address;
        if(!state.getSymTable().isDefined(this.variable))
            throw new MyException("ERROR: There is no \""+this.variable+"\" in SymTable !!!");
        else
            address=state.getSymTable().lookup(this.variable);

        if(!state.getHeap().isKey(address))
            throw new MyException("ERROR: Invalid address exception !!!");
        else
        {
            int val=this.expression.eval(state.getSymTable(),state.getHeap());
            state.getHeap().add(address,val);
        }

        return null;
    }

    @Override
    public String toString(){return "wH("+this.variable.toString()+","+this.expression.toString()+")";}
}
