package Model;

import java.io.IOException;

public class WhileStmt implements IStmt {
    private Exp exp;
    private IStmt statement;

    public WhileStmt(Exp e,IStmt s){
        this.exp=e;
        this.statement=s;
    }


    @Override
    public  PrgState execute(PrgState state){
        if(this.exp.eval(state.getSymTable(),state.getHeap())!=0){
            state.getExeStack().push(new WhileStmt(this.exp,this.statement));
            state.getExeStack().push(this.statement);
        }

        return null;
    }

    @Override
    public String toString(){
        return  "(while("+this.exp.toString()+")"+this.statement.toString()+")";
    }
}
