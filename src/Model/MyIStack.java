package Model;

import java.util.Stack;

public interface MyIStack<T> {
    T pop();
    void push(T el);
    boolean isEmpty();
    T top();
    String toString();
}


