package Model;

public class BooleanExp extends Exp {
    private Exp exp1,exp2;
    private int op;

    public BooleanExp(Exp e1,Exp e2,int o){
        this.exp1=e1;
        this.exp2=e2;
        this.op=o;
    }

    @Override
    public int eval(MyIDictionary<String,Integer> table,MyIHeap<Integer,Integer> heap){
        if(this.op==1)
        {
            if(this.exp1.eval(table,heap)<this.exp2.eval(table,heap))
                return 1;
            else
                return 0;
        }
        else if(op==2){
            if(this.exp1.eval(table,heap)<=this.exp2.eval(table,heap))
                return 1;
            else
                return 0;
        }
        else if(op==3){
            if(this.exp1.eval(table,heap)==this.exp2.eval(table,heap))
                return 1;
            else
                return 0;
        }
        else if(op==4){
            if(this.exp1.eval(table,heap)!=this.exp2.eval(table,heap))
                return 1;
            else
                return 0;
        }
        else if(op==5){
            if(this.exp1.eval(table,heap)>this.exp2.eval(table,heap))
                return 1;
            else
                return 0;
        }
        else if(op==6){
            if(this.exp1.eval(table,heap)>=this.exp2.eval(table,heap))
                return 1;
            else
                return 0;
        }throw new MyException("ERROR: Undefined operator !!!");
    }

    @Override
    public String toString(){
        String operation;
        if(this.op==1)
            operation="<";
        else if(this.op==2)
            operation="<=";
        else if(this.op==3)
            operation="==";
        else if(this.op==4)
            operation="!=";
        else if(this.op==5)
            operation=">";
        else
            operation=">=";

        return this.exp1.toString()+operation+this.exp2.toString();
    }
}
