package Model;

public class ArithExp extends Exp {
    private Exp e1;
    private Exp e2;
    private int op;

    public ArithExp(Exp _e1, Exp _e2, int _op) {
        this.e1 = _e1;
        this.e2 = _e2;
        this.op = _op;
    }

    @Override
    public int eval(MyIDictionary<String, Integer> tbl,MyIHeap<Integer,Integer> heap){

        if (this.op == 1)
            return (this.e1.eval(tbl,heap) + this.e2.eval(tbl,heap));
        else if (this.op == 2)
            return (this.e1.eval(tbl,heap) - this.e2.eval(tbl,heap));
        else if (this.op == 3)
            return (this.e1.eval(tbl,heap) * this.e2.eval(tbl,heap));
        else if (this.op == 4) {
            if (this.e2.eval(tbl,heap) == 0) throw new MyException("ERROR: Division by 0 !!!\n");
            else
                return (this.e1.eval(tbl,heap) / this.e2.eval(tbl,heap));
        }
        throw new MyException("ERROR: Undefined operator !!!\n");
    }

    @Override
    public String toString(){
        String buffer="Eval ("+e1.toString();
        String operation;
        if(op==1)
            operation="+";
        else if(op==2)
            operation="-";
        else if(op==3)
            operation="*";
        else
            operation="/";

        buffer=buffer+operation+e2.toString()+")= Eval("+e1.toString()+")"+operation+"Eval("+e2.toString()+")";

        return buffer;
    }

}
