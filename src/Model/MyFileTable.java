package Model;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class MyFileTable implements MyIFileTable<Integer,MyTuple<String, BufferedReader>> {
    private HashMap<Integer,MyTuple<String,BufferedReader>> fileTable;
    private int key;

    public MyFileTable(){
        this.fileTable=new HashMap<Integer,MyTuple<String,BufferedReader>>();
        this.key=0;
    }

    @Override
    public void add(Integer k,MyTuple<String,BufferedReader> tuple){
        this.fileTable.put(k,tuple);
    }

    @Override
    public void remove(int k){
        this.fileTable.remove(k);
    }

    @Override
    public void incrementKey(){
        this.key++;
    }

    @Override
    public Integer existsFile(String filename){
        for(Map.Entry<Integer,MyTuple<String,BufferedReader>> i:this.fileTable.entrySet())
            if(Objects.equals(filename,i.getValue().getA()))
                return i.getKey();

        return null;
    }

    @Override
    public int getKey(){
        return this.key;
    }

    @Override
    public BufferedReader getBufferedReader(int key) {
        return this.fileTable.get(key).getB();
    }

    @Override
    public String toString(){
        return this.fileTable.toString();
    }

    @Override
    public MyTuple<String,BufferedReader> lookup(Integer id){
        return this.fileTable.get(id);
    }

    @Override
    public void closeAll(){
        BufferedReader br;
        for(Map.Entry<Integer,MyTuple<String,BufferedReader>> i:this.fileTable.entrySet()){
            br=i.getValue().getB();
            try {
                br.close();
            }catch (IOException ioe){
                System.out.println("ERROR: I/O Exception. Cannot close file !!!");
            }
        }
        this.fileTable.clear();
    }

    @Override
    public Collection<MyTuple<String,BufferedReader>> getValues(){
        return fileTable.values();
    }

    @Override
    public void setContent(HashMap<Integer,MyTuple<String,BufferedReader>> newFileTable){
        fileTable=new HashMap<>();
        for(Integer i:newFileTable.keySet()){
            fileTable.put(i,newFileTable.get(i));
        }
    }

    @Override
    public HashMap<Integer,MyTuple<String,BufferedReader>> getContent(){
        return this.fileTable;
    }

    @Override
    public Collection<Integer> getKeySet(){
        return this.fileTable.keySet();
    }

    @Override
    public void clearContent(){
        this.fileTable.clear();
    }
}
