package Model;

import java.io.BufferedReader;
import java.lang.management.BufferPoolMXBean;

public class ForkStmt implements IStmt {
    private IStmt statement;

    public ForkStmt(IStmt stmt){
        this.statement=stmt;
    }

    @Override
    public PrgState execute(PrgState state){
        MyIStack<IStmt> newStack=new MyStack<>();
        MyIDictionary<String,Integer> newDictionary=state.getSymTable().cloneDictionary();

        PrgState prgS= new PrgState(newStack,newDictionary,state.getOut(),state.getFileTable(),
         state.getHeap(),this.statement,state.getId());
        //prgS.decrementCounter();
        prgS.setId((prgS.getId())+1);

        return prgS;
    }

    @Override
    public String toString(){
        return "ForkStmt("+this.statement.toString()+")";
    }
}
