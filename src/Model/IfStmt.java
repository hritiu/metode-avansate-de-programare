package Model;

import java.io.IOException;

public class IfStmt implements IStmt{
    private Exp exp;
    private IStmt thenS;
    private IStmt elseS;

    public IfStmt(Exp _e,IStmt _t,IStmt _el){
        this.exp=_e;
        this.thenS=_t;
        this.elseS=_el;
    }

    public String toString(){
        return "IF("+this.exp.toString()+") THEN("+this.thenS.toString()+
                ") ELSE ("+this.elseS.toString()+")";
    }

    public PrgState execute(PrgState state)throws IOException {
        MyIStack<IStmt> stk=state.getExeStack();
        MyIDictionary<String,Integer> symTbl=state.getSymTable();
        try {
            int val = exp.eval(symTbl,state.getHeap());
            if (val != 0)
                stk.push(this.thenS);
            else
                stk.push(this.elseS);
        }catch (MyException exception){
            System.out.println(exception.getMessage());
        }
        return null;
    }
}
