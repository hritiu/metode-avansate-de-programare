package Model;

public class VarExp extends Exp{
    private String id;

    public VarExp(String _id){this.id=_id;}

    @Override
    public int eval(MyIDictionary<String,Integer> tbl,MyIHeap<Integer,Integer> heap)
            throws MyException{
        if(tbl.isDefined(this.id)){
            return tbl.lookup(this.id);
        }
        else throw new MyException("ERROR: Variable not found !!!\n");
    }

    @Override
    public String toString(){return "Eval("+this.id+")=LookUp(SymTable,"+this.id+")"; }
}
