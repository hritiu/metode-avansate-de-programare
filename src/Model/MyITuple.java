package Model;

public interface MyITuple<A,B> {
    void setA(A aa);
    void setB(B bb);
    A getA();
    B getB();
    String toString();
}
