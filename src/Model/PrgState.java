package Model;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.annotation.Documented;
import java.util.prefs.PreferencesFactory;

public class PrgState implements IPrgState {
    private MyIStack<IStmt> exeStack;
    private MyIDictionary<String,Integer> symTable;
    private MyIList<Integer> out;
    private MyIFileTable<Integer,MyITuple<String, BufferedReader>> fileT;
    private MyIHeap<Integer,Integer> heap;
    private IStmt originaProgram;
    private int id;
    private  int counter=0;

    public PrgState(MyIStack<IStmt> stk,MyIDictionary<String,Integer> symtbl,MyIList<Integer> ot,
            MyIFileTable<Integer,MyITuple<String,BufferedReader>> f,MyIHeap<Integer,Integer> h,
                    IStmt prg,int _id){
        this.exeStack=stk;
        this.symTable=symtbl;
        this.out=ot;
        this.fileT=f;
        this.heap=h;
        this.exeStack.push(prg);
        //this.id=++counter;
        this.id=_id++;
    }

    @Override
    public void setExeStack(MyIStack<IStmt> _exeStack){
        this.exeStack=_exeStack;
    }

    @Override
    public void setSymTable(MyIDictionary<String,Integer> _symTable){
        this.symTable=_symTable;
    }

    @Override
    public void setOut(MyIList<Integer> _out){
        this.out=_out;
    }

    @Override
    public void setFileTable(MyIFileTable<Integer,MyITuple<String, BufferedReader>> _f){this.fileT=_f;}

    @Override
    public void setHeap(MyIHeap<Integer,Integer> _h){this.heap=_h;}

    @Override
    public void setId(int _id){this.id=_id;}

    @Override
    public MyIStack<IStmt> getExeStack(){
        return this.exeStack;
    }

    @Override
    public MyIDictionary<String,Integer> getSymTable(){
        return this.symTable;
    }

    @Override
    public MyIList<Integer> getOut(){
        return this.out;
    }

    @Override
    public MyIFileTable<Integer,MyITuple<String,BufferedReader>> getFileTable(){return this.fileT;}

    @Override
    public MyIHeap<Integer,Integer> getHeap(){return this.heap;}

    @Override
    public int getId(){return this.id;}

    @Override
    public String toString(){
        return "Id="+Integer.toString(this.id)+"\n"+"ExecutionStack="+this.exeStack.toString()+"\n"+"SymbolTable="+this.symTable.toString()+"\n"+"Out="+
                this.out.toString()+"\n"+"File Table"+this.fileT.toString()+"\n"+"Heap"+this.heap.toString()+"\n";
    }

    @Override
    public boolean isNotCompleted(){
        if(this.exeStack.isEmpty())
            return false;
        return true;
    }

    @Override
    public PrgState oneStep() throws IOException {
        if(this.exeStack.isEmpty())
            throw new MyException("ERROR: Stack is empty !!!");

        IStmt crtStmt=this.exeStack.pop();
        return crtStmt.execute(this);
    }

    @Override
    public void decrementCounter(){
        counter--;
    }
}
