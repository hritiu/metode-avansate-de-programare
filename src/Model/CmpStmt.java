package Model;

import java.io.IOException;

public class CmpStmt implements IStmt {
    private IStmt first;
    private IStmt snd;

    public CmpStmt(IStmt _first,IStmt _snd){this.first=_first;this.snd=_snd;}

    @Override
    public String toString(){
        return "("+first.toString()+"|"+snd.toString()+")";
    }

    @Override
    public PrgState execute(PrgState state) throws IOException {
        MyIStack<IStmt> stk=state.getExeStack();
        stk.push(this.snd);
        stk.push(this.first);

        return null;
    }
}
