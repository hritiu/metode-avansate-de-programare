package Model;

public class PrintStmt implements IStmt {
    private Exp exp;

    public PrintStmt(Exp _exp){this.exp=_exp;}

    @Override
    public String toString(){
        return "print{"+this.exp.toString()+"}";
    }

    @Override
    public PrgState execute(PrgState state) throws MyException{
        MyIDictionary<String,Integer> symTbl=state.getSymTable();
        MyIList<Integer> oUt=state.getOut();

        int val=this.exp.eval(symTbl,state.getHeap());
        oUt.add(val);

        return null;
    }
}
