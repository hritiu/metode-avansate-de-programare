package Model;

public class ConstExp extends Exp{
    private int number;

    public ConstExp(int _number){this.number=_number;}

    @Override
    public int eval(MyIDictionary<String,Integer> tbl,MyIHeap<Integer,Integer> heap){

        return this.number;
    }

    @Override
    public String toString(){
        return Integer.toString(this.number)+"";
    }
}
