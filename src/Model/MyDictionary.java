package Model;

import java.util.*;

public class MyDictionary<K,V> implements MyIDictionary<K,V> {
    private HashMap<K,V> dict;

    public MyDictionary() {this.dict=new HashMap<>();}

    @Override
    public void add(K k,V v){
        //this.dictionary.put(k,v);
        this.dict.put(k,v);
    }

    @Override
    public void addAll(HashMap<K,V> dict){
        this.dict.putAll(dict);
    }

    @Override
    public void remove(K k){
        //if(!isEmpty())
            //dictionary.remove(k);
        if(!isEmpty())
            this.dict.remove(k);
    }

    @Override
    public V lookup(K k){
         return this.dict.get(k);
    }

    @Override
    public boolean isEmpty(){

        //return dictionary.isEmpty();
        return this.dict.isEmpty();
    }

    @Override
    public String toString(){
        return this.dict.toString();
    }

    @Override
    public boolean isDefined(K id){

        return this.dict.containsKey(id);
    }

    @Override
    public void update(K id,V val){
        this.dict.put(id,val);
    }

    @Override
    public int getSize(){return this.dict.size();}

    @Override
    public Set<K> getKeys(){return this.dict.keySet();}

    @Override
    public Collection<V> getValues(){return  this.dict.values();}

    @Override
    public HashMap<K,V> getContent(){
        return this.dict;
    }

    @Override
    public MyIDictionary<K,V> cloneDictionary(){
        MyIDictionary<K,V> newDictionary=new MyDictionary<K,V>();
        newDictionary.addAll(this.dict);

//        for(Map.Entry<K,V> i:dict.entrySet())
//            newDictionary.add(i.getKey(),i.getValue());
        return newDictionary;
    }
}
