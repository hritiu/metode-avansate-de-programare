package Model;

public class rHExp extends Exp {
    private String variable;

    public rHExp(String var){this.variable=var;}


    @Override
    public int eval(MyIDictionary<String,Integer> tbl,MyIHeap<Integer,Integer> heap){
        int val,key;

        if(tbl.lookup(this.variable)==null)
            throw new MyException("ERROR: Variable not found in Symbol Table !!!");
        else {
            key=tbl.lookup(this.variable);

            if(!heap.isKey(key))
                throw new MyException("ERROR: key not found in Heap !!!");
            else
                val=heap.lookup(key);

            return val;
        }
    }

    @Override
    public String toString(){
        return "rH("+this.variable.toString()+")";
    }
}
