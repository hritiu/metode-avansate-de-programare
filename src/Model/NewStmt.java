package Model;

public class NewStmt implements IStmt {
    private String variable;
    private Exp expression;

    public NewStmt(String var,Exp e){
        this.expression=e;
        this.variable=var;
    }

    @Override
    public PrgState execute(PrgState state){
        int key,val;

        if(state.getSymTable().lookup(this.variable)==null){
            state.getHeap().incrementFreeLocation();
            key=state.getHeap().getFreeLocation();
            state.getSymTable().add(this.variable,key);
        }
        else {
            state.getSymTable().add(this.variable,state.getHeap().getFreeLocation());
            key = state.getSymTable().lookup(this.variable);
        }

        val=this.expression.eval(state.getSymTable(),state.getHeap());

        state.getHeap().add(key,val);

        return null;
    }

    @Override
    public String toString(){
        return "new("+this.variable.toString()+","+this.expression.toString()+")";
    }
}
