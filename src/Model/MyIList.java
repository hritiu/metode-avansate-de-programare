package Model;

public interface MyIList<T> {
    void add(T val);
    void remove();
    T get(int pos);
    boolean isEmpty();
    String toString();
    int getSize();
}
