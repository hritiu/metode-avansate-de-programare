package Model;

import java.util.ArrayList;

public class MyList<T> implements MyIList<T> {
    //private List<T> list;
    private ArrayList<T> list;

    public MyList(){this.list=new ArrayList<>();}

    @Override
    public void add(T val){
        this.list.add(val);
    }

    @Override
    public void remove(){
        list.remove(list.size()-1);
    }

    @Override
    public  T get(int pos){
        return list.get(pos);
    }

    @Override
    public boolean isEmpty(){
        return list.isEmpty();
    }

    @Override
    public String toString(){
//        String buffer="{";
//
//        for(int i=0;i<list.size();i++){
//            buffer=buffer+get(i);
//            buffer+=",";
//        }
//        //buffer=buffer.substring(0,buffer.length()-1);
//        buffer=buffer+"};";
//        return buffer;

        return this.list.toString();
    }

    @Override
    public int getSize(){return this.list.size();}
}
