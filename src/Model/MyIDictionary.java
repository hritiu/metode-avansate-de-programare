package Model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

public interface MyIDictionary<K,V> {
    void add(K k,V v);
    void addAll(HashMap<K,V> dict);
    void remove(K k);
    V lookup(K k);
    boolean isEmpty();
    String toString();
    boolean isDefined(K id);
    void update(K id,V val);
    int getSize();
    Set<K> getKeys();
    Collection<V> getValues();
    HashMap<K,V> getContent();
    MyIDictionary<K,V> cloneDictionary();
}