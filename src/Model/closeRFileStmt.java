package Model;

import java.io.BufferedReader;
import java.io.IOException;

public class closeRFileStmt implements IStmt {
    private Exp exp_file_id;

    public closeRFileStmt(Exp e){
        this.exp_file_id=e;
    }

    @Override
    public PrgState execute(PrgState state) throws IOException{
        try {
            int val = this.exp_file_id.eval(state.getSymTable(),state.getHeap());
            try {
                BufferedReader br = state.getFileTable().getBufferedReader(val);
                br.close();
                state.getFileTable().remove(val);
            }catch (MyException me2){
                System.out.println("ERROR: There is no BufferedReader associated with this key !!!\n");
            }catch (IOException ioe){
                System.out.println("ERROR: Cannot close file !!!\n");
            }
        }catch (MyException me){
            System.out.println("ERROR: Cannot evaluate expression !!!\n");
        }

//        MyIDictionary<String,Integer> dictionary=state.getSymTable();
//        MyIFileTable<Integer,MyITuple<String,BufferedReader>> fileT=state.getFileTable();
//        int val=this.exp_file_id.eval(dictionary);
//        BufferedReader br=state.getFileTable().lookup(val).getB();
//        if(br==null)
//            throw new MyException("No BufferedReader found at closing");
//        br.close();
//        fileT.remove(val);

        return  null;
    }

    @Override
    public String toString(){return "closeRFile("+this.exp_file_id.toString()+")";}
}
