package Model;

import java.io.BufferedReader;
import java.io.IOException;

public interface IPrgState {
    void setExeStack(MyIStack<IStmt> _exeStack);
    void setSymTable(MyIDictionary<String,Integer> _symTable);
    void setOut(MyIList<Integer> _out);
    void setFileTable(MyIFileTable<Integer,MyITuple<String, BufferedReader>> _f);
    void setHeap(MyIHeap<Integer,Integer> _h);
    MyIStack<IStmt> getExeStack();
    MyIDictionary<String,Integer> getSymTable();
    MyIList<Integer> getOut();
    MyIFileTable<Integer,MyITuple<String,BufferedReader>> getFileTable();
    MyIHeap<Integer,Integer> getHeap();
    String toString();
    boolean isNotCompleted();
    PrgState oneStep() throws IOException;
    void setId(int _id);
    int getId();
    void decrementCounter();
}
