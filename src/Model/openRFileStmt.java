package Model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class openRFileStmt implements IStmt {
    private String filename,var_file_id;

    public openRFileStmt(String id,String name){this.var_file_id=id;this.filename=name;}

    @Override
    public PrgState execute(PrgState state)throws MyException,IOException{
        MyIFileTable<Integer,MyITuple<String,BufferedReader>> fileT=state.getFileTable();
        if(state.getFileTable().existsFile(this.filename)!=null)
            throw new MyException("File already exists in FileTable!");

//        try {
//            BufferedReader br = new BufferedReader(new FileReader(this.filename));
//            int id=state.getFileTable().getKey();
//            MyTuple<String,BufferedReader> tuple=new MyTuple<>(this.filename,br);
//            state.getFileTable().add(id,tuple);
//            state.getFileTable().incrementKey();
//        }catch (FileNotFoundException fnfe){System.out.println("File not found!");}


        BufferedReader br = new BufferedReader(new FileReader(this.filename));

        int id=fileT.getKey();
        fileT.incrementKey();
        MyTuple<String,BufferedReader> tuple=new MyTuple<>(this.filename,br);
        fileT.add(id,tuple);

        state.getSymTable().add(this.var_file_id,id);
        return null;
    }

    @Override
    public String toString(){return "openRFile("+this.var_file_id+","+this.filename+")";}
}
