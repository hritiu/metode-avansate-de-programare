package Model;

import java.io.BufferedReader;
import java.util.Collection;
import java.util.HashMap;

public interface MyIFileTable<K,T>{
    void add(Integer k,T t);
    void remove(int k);
    void incrementKey();
    Integer existsFile(String filename);
    int getKey();
    BufferedReader getBufferedReader(int key);
    String toString();
    T lookup(K id);
    void closeAll();
    Collection<MyTuple<String,BufferedReader>> getValues();
    void setContent(HashMap<Integer,MyTuple<String,BufferedReader>> newFileTable);
    HashMap<Integer,MyTuple<String,BufferedReader>> getContent();
    Collection<Integer> getKeySet();
    void clearContent();
}
