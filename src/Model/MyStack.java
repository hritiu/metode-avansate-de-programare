package Model;

import java.util.Stack;

public class MyStack<T> implements MyIStack<T> {
    private Stack<T> stack;

    public MyStack(){
        this.stack=new Stack<>();
    }

    @Override
    public void push(T el){
        this.stack.push(el);
    }

    @Override
    public T pop(){
        return this.stack.pop();
    }

    @Override
    public boolean isEmpty(){
        return this.stack.isEmpty();
    }

    @Override
    public T top(){
        return this.stack.peek();
    }

    @Override
    public String toString(){

        String buffer="";
        String finalb="";
        for(T r:stack){
            buffer+=r;
            finalb=buffer+" | "+finalb;
            buffer="";
        }
        if(finalb.length()>3)
            finalb=finalb.substring(0,finalb.length()-3);
        finalb="{"+finalb+"}";

        return finalb;
    }
}
