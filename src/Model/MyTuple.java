package Model;

public class MyTuple<A,B> implements MyITuple<A,B> {
    private A a;
    private B b;

    public MyTuple(A aa,B bb){this.a=aa;this.b=bb;}

    @Override
    public void setA(A aa){this.a=aa;}

    @Override
    public void setB(B bb){this.b=bb;}

    @Override
    public A getA(){return this.a;}

    @Override
    public B getB(){return  this.b;}

    @Override
    public String toString(){
        return "Tuple("+a.toString()+","+b.toString()+")";
    }
}