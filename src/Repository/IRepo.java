package Repository;

import Model.PrgState;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public interface IRepo {
    //PrgState getCrtPrg();
    void add(PrgState prg);
    void logPrgStateExec(PrgState programState);
    ArrayList<PrgState> getPrgList();
    void setPrgList(List<PrgState> newPrgList);
}
