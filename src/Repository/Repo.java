package Repository;

import Model.*;

import java.io.*;
import java.util.*;

public class Repo implements IRepo {
    private ArrayList<PrgState> elems;
    private String logPathFile;

    public Repo(String logFile){
        this.elems=new ArrayList<>();
        this.logPathFile=logFile;
    }

    public void setPathFile(String path){this.logPathFile=path;}

//    @Override
//    public PrgState getCrtPrg(){
//        return this.elems.get(0);
//    }

    @Override
    public void add(PrgState prg){
        this.elems.add(prg);
    }

    @Override
    public void logPrgStateExec(PrgState programState) {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(this.logPathFile, true));
            PrintWriter logFile = new PrintWriter(bw);

            logFile.append("Id:"+Integer.toString(programState.getId()));
            bw.newLine();
            logFile.append("");

            logFile.append("Execution Stack");
            bw.newLine();
            logFile.append(programState.getExeStack().toString());
            bw.newLine();
            logFile.append("");

            logFile.append("Symbol Table");
            bw.newLine();
            logFile.append(programState.getSymTable().toString());
            bw.newLine();
            logFile.append("");

            logFile.append("Output");
            bw.newLine();
            logFile.append(programState.getOut().toString());
            bw.newLine();
            logFile.append("");

            logFile.append("File table");
            bw.newLine();
            logFile.append(programState.getFileTable().toString());
            bw.newLine();
            logFile.append("");

            logFile.append("Heap");
            bw.newLine();
            logFile.append(programState.getHeap().toString());
            bw.newLine();

            logFile.append("");
            bw.newLine();
            bw.newLine();
            logFile.close();
        }catch (IOException ioe){
            System.out.println("ERROR: Cannot open file !!!");
        }
    }

    @Override
    public ArrayList<PrgState> getPrgList(){
        return this.elems;
    }

    @Override
    public void setPrgList(List<PrgState> newPrgList){
        this.elems.clear();
        this.elems.addAll(newPrgList);
    }
}

