package View;

import Controller.Controller;
import Model.*;
import Repository.IRepo;
import Repository.Repo;

import java.time.chrono.IsoChronology;
import java.util.IllegalFormatCodePointException;

public class App {
    public static void main(String[] args){
        //Ex1
        // v=2;Print(v);
        IStmt stmt1=new CmpStmt(new AssignStmt("v",new ConstExp(2)),new PrintStmt(new VarExp("v")));
        MyIStack stack1=new MyStack<IStmt>();
        MyIDictionary<String,Integer> dictionary1=new MyDictionary<>();
        MyIList list1=new MyList<Integer>();
        MyIFileTable fileTable1=new MyFileTable();
        MyIHeap heap1=new MyHeap();

        PrgState prgState1=new PrgState(stack1,dictionary1,list1,fileTable1,heap1,stmt1,1);
        IRepo repo1=new Repo("example.out.txt");
        repo1.add(prgState1);
        Controller ctrl1=new Controller(repo1);

        //Ex2
        // a=2+3*5;b=a+1;Print(b)
        AssignStmt a2=new AssignStmt("a",new ArithExp(new ConstExp(2),new ArithExp(new ConstExp(3),new ConstExp(5),3),1));
        AssignStmt b2=new AssignStmt("b",new ArithExp(new VarExp("a"),new ConstExp(1),1));
        //IStmt stmt2=new CmpStmt(new CmpStmt(a2,b2),new PrintStmt(new VarExp("b")));
        IStmt stmt2=new CmpStmt(a2,new CmpStmt(b2,new PrintStmt(new VarExp("b"))));

        MyIStack stack2=new MyStack<IStmt>();
        MyIDictionary<String,Integer> dictionary2=new MyDictionary<>();
        MyIList list2=new MyList<Integer>();
        MyIFileTable fileTable2=new MyFileTable();
        MyIHeap heap2=new MyHeap();

        PrgState prgState2=new PrgState(stack2,dictionary2,list2,fileTable2,heap2,stmt2,1);
        IRepo repo2=new Repo("example.out.txt");
        repo2.add(prgState2);
        Controller ctrl2=new Controller(repo2);

        //Ex3
        // a=2-2;(If a Then v=2 Else v=3);Print(v)
        AssignStmt a3=new AssignStmt("a",new ArithExp(new ConstExp(2),new ConstExp(2),2));
        IfStmt i3=new IfStmt(new VarExp("a"),new AssignStmt("v",new ConstExp(2)),new AssignStmt("v",new ConstExp(3)));
        PrintStmt p3=new PrintStmt(new VarExp("v"));
        //IStmt stmt3=new CmpStmt(new CmpStmt(a3,i3),p3);
        IStmt stmt3=new CmpStmt(a3,new CmpStmt(i3,p3));

        MyIStack stack3=new MyStack<IStmt>();
        MyIDictionary<String,Integer> dictionary3=new MyDictionary<>();
        MyIList list3=new MyList<Integer>();
        MyIFileTable fileTable3=new MyFileTable();
        MyIHeap heap3=new MyHeap();

        PrgState prgState3=new PrgState(stack3,dictionary3,list3,fileTable3,heap3,stmt3,1);
        IRepo repo3=new Repo("example.out.txt");
        repo3.add(prgState3);
        Controller ctrl3=new Controller(repo3);



        //Ex4:
        // openRFile(var_f,"test.in");
        // readFile(var_f+2,var_c);print(var_c);
        // (if var_c then readFile(var_f,var_c);print(var_c)
        // else print(0));
        // closeRFile(var_f)
        Exp var_f=new VarExp("var_f");
        Exp var_c=new VarExp("var_c");
        openRFileStmt open4=new openRFileStmt("var_f","example.in.txt");
        readFileStmt read4=new readFileStmt(var_f,"var_c");
        IStmt readPrint=new CmpStmt(read4,new PrintStmt(var_c));
        IStmt if4=new IfStmt(var_c,new CmpStmt(read4,new PrintStmt(var_c)),new PrintStmt(new ConstExp(0)));
        closeRFileStmt close4=new closeRFileStmt(var_f);
        //IStmt stmt4=new CmpStmt(new CmpStmt(open4,readPrint),new CmpStmt(if4,close4));
        IStmt stmt4=new CmpStmt(open4,new CmpStmt(readPrint,new CmpStmt(if4,close4)));

        MyIStack stack4=new MyStack<IStmt>();
        MyIDictionary<String,Integer> dictionary4=new MyDictionary<>();
        MyIList list4=new MyList<Integer>();
        MyIFileTable fileTable4=new MyFileTable();
        MyIHeap heap4=new MyHeap();

        PrgState prgState4=new PrgState(stack4,dictionary4,list4,fileTable4,heap4,stmt4,1);
        IRepo repo4=new Repo("example.out.txt");
        repo4.add(prgState4);
        Controller ctrl4=new Controller(repo4);


        //Ex5
        // openRFile
        //(var_f,"test.in");
        // readFile(var_f+2,var_c);print(var_c);
        // (if var_c then readFile(var_f,var_c);print(var_c)
        // else print(0));
        // closeRFile(var_f)
        Exp var_f5=new VarExp("var_f5");
        Exp var_c5=new VarExp("var_c5");
        openRFileStmt open5=new openRFileStmt("var_f5","example.in.txt");
        readFileStmt read5=new readFileStmt(new ArithExp(var_f5,new ConstExp(2),1),"var_c5");
        IStmt readPrint5=new CmpStmt(new readFileStmt(var_f5,"var_c5"),new PrintStmt(var_c5));
        IStmt i5=new IfStmt(var_c5,new CmpStmt(read5,new PrintStmt(var_c5)),new PrintStmt(new ConstExp(0)));
        closeRFileStmt close5=new closeRFileStmt(var_f5);
        //IStmt stmt5=new CmpStmt(new CmpStmt(open5,readPrint5),new CmpStmt(i5,close5));
        IStmt stmt5=new CmpStmt(open5,new CmpStmt(readPrint5,new CmpStmt(i5,close5)));

        MyIStack stack5=new MyStack<IStmt>();
        MyIDictionary<String,Integer> dictionary5=new MyDictionary<>();
        MyIList list5=new MyList<Integer>();
        MyIFileTable fileTable5=new MyFileTable();
        MyIHeap heap5=new MyHeap();

        PrgState prgState5=new PrgState(stack5,dictionary5,list5,fileTable5,heap5,stmt5,1);
        IRepo repo5=new Repo("example.out.txt");
        repo5.add(prgState5);
        Controller ctrl5=new Controller(repo5);

        //Ex6: WITH EMPTY INPUT FILE
        // openRFile(var_f,"test.in");
        // readFile(var_f+2,var_c);print(var_c);
        // (if var_c then readFile(var_f,var_c);print(var_c)
        // else print(0));
        // closeRFile(var_f)
        Exp var_f6=new VarExp("var_f6");
        Exp var_c6=new VarExp("var_c6");
        openRFileStmt open6=new openRFileStmt("var_f6","emptyExample.in.txt");
        readFileStmt read6=new readFileStmt(var_f6,"var_c6");
        IStmt readPrint6=new CmpStmt(read6,new PrintStmt(var_c6));
        IStmt if6=new IfStmt(var_c6,new CmpStmt(read6,new PrintStmt(var_c6)),new PrintStmt(new ConstExp(0)));
        closeRFileStmt close6=new closeRFileStmt(var_f6);
        //IStmt stmt6=new CmpStmt(new CmpStmt(open6,readPrint6),new CmpStmt(if6,close6));
        IStmt stmt6=new CmpStmt(open6,new CmpStmt(readPrint6,new CmpStmt(if6,close6)));

        MyIStack stack6=new MyStack<IStmt>();
        MyIDictionary<String,Integer> dictionary6=new MyDictionary<>();
        MyIList list6=new MyList<Integer>();
        MyIFileTable fileTable6=new MyFileTable();
        MyIHeap heap6=new MyHeap();

        PrgState prgState6=new PrgState(stack6,dictionary6,list6,fileTable6,heap6,stmt6,1);
        IRepo repo6=new Repo("example.out.txt");
        repo6.add(prgState6);
        Controller ctrl6=new Controller(repo6);

        //Ex7:  NewStmt, wHStmt, rHStmt
        // v=10;new(v,20);new(a,22);wH(a,30);print(a);print(rH(a));a=0
        AssignStmt v7=new AssignStmt("v",new ConstExp(10));
        NewStmt n7=new NewStmt("v",new ConstExp(20));
        NewStmt a7=new NewStmt("a",new ConstExp(22));
        wHStmt w7=new wHStmt("a",new ConstExp(30));
        PrintStmt p7=new PrintStmt(new VarExp("a"));
        PrintStmt pp7=new PrintStmt(new rHExp("a"));
        AssignStmt aa7=new AssignStmt("a",new ConstExp(0));
        //IStmt stmt7=new CmpStmt(new CmpStmt(new CmpStmt(new CmpStmt(new CmpStmt(new CmpStmt(new CmpStmt(v7,n7),a7),w7),p7),pp7),aa7),pp7);
        IStmt stmt7=new CmpStmt(v7,new CmpStmt(n7,new CmpStmt(a7,new CmpStmt(w7,new CmpStmt(p7,new CmpStmt(pp7,new CmpStmt(aa7,pp7)))))));

        MyIStack stack7=new MyStack<IStmt>();
        MyIDictionary<String,Integer> dictionary7=new MyDictionary<>();
        MyIList list7=new MyList<Integer>();
        MyIFileTable fileTable7=new MyFileTable();
        MyIHeap heap7=new MyHeap();

        PrgState prgState7=new PrgState(stack7,dictionary7,list7,fileTable7,heap7,stmt7,1);
        IRepo repo7=new Repo("example.out.txt");
        repo7.add(prgState7);
        Controller ctrl7=new Controller(repo7);

        //Ex8: NewStmt
        // v=10;new(v,20);new(a,22);print(v)
        AssignStmt a8=new AssignStmt("v",new ConstExp(20));
        NewStmt n8=new NewStmt("v",new ConstExp(20));
        NewStmt nn8=new NewStmt("a",new ConstExp(22));
        PrintStmt p8=new PrintStmt(new VarExp("v"));
        //IStmt stmt8=new CmpStmt(new CmpStmt(new CmpStmt(a8,n8),nn8),p8);
        IStmt stmt8=new CmpStmt(a8,new CmpStmt(n8,new CmpStmt(nn8,p8)));

        MyIStack stack8=new MyStack<IStmt>();
        MyIDictionary<String,Integer> dictionary8=new MyDictionary<>();
        MyIList list8=new MyList<Integer>();
        MyIFileTable fileTable8=new MyFileTable();
        MyIHeap heap8=new MyHeap();

        PrgState prgState8=new PrgState(stack8,dictionary8,list8,fileTable8,heap8,stmt8,1);
        IRepo repo8=new Repo("example.out.txt");
        repo8.add(prgState8);
        Controller ctrl8=new Controller(repo8);

        //Ex9: rHStmt
        // v=10;new(v,20);new(a,22);print(100+rH(v));print(100+rH(a));
        ArithExp e9=new ArithExp(new ConstExp(100),new rHExp("v"),1);
        PrintStmt p9=new PrintStmt(e9);
        ArithExp ee9=new ArithExp(new ConstExp(100),new rHExp("a"),1);
        PrintStmt pp9=new PrintStmt(ee9);
        AssignStmt v9=new AssignStmt("v",new ConstExp(10));
        NewStmt n9=new NewStmt("v",new ConstExp(20));
        NewStmt nn9=new NewStmt("a",new ConstExp(22));
        //IStmt stmt9=new CmpStmt(new CmpStmt(new CmpStmt(new CmpStmt(v9,n9),nn9),p9),pp9);
        IStmt stmt9=new CmpStmt(v9,new CmpStmt(n9,new CmpStmt(nn9,new CmpStmt(p9,pp9))));

        MyIStack stack9=new MyStack<IStmt>();
        MyIDictionary<String,Integer> dictionary9=new MyDictionary<>();
        MyIList list9=new MyList<Integer>();
        MyIFileTable fileTable9=new MyFileTable();
        MyIHeap heap9=new MyHeap();

        PrgState prgState9=new PrgState(stack9,dictionary9,list9,fileTable9,heap9,stmt9,1);
        IRepo repo9=new Repo("example.out.txt");
        repo9.add(prgState9);
        Controller ctrl9=new Controller(repo9);

        //Ex10: wHStmt
        //v=10;new(v,20);new(a,22);wH(a,30);print(a);print(rH(a));
        AssignStmt a10=new AssignStmt("v",new ConstExp(10));
        NewStmt n10=new NewStmt("v",new ConstExp(20));
        NewStmt nn10=new NewStmt("a",new ConstExp(22));
        wHStmt w10=new wHStmt("a",new ConstExp(30));
        PrintStmt p10=new PrintStmt(new VarExp("a"));
        PrintStmt pp10=new PrintStmt(new rHExp("a"));
        //IStmt stmt10=new CmpStmt(new CmpStmt(new CmpStmt(new CmpStmt(new CmpStmt(a10,n10),nn10),w10),p10),pp10);
        IStmt stmt10=new CmpStmt(a10,new CmpStmt(n10,new CmpStmt(nn10,new CmpStmt(w10,new CmpStmt(p10,pp10)))));

        MyIStack stack10=new MyStack<IStmt>();
        MyIDictionary<String,Integer> dictionary10=new MyDictionary<>();
        MyIList list10=new MyList<Integer>();
        MyIFileTable fileTable10=new MyFileTable();
        MyIHeap heap10=new MyHeap();

        PrgState prgState10=new PrgState(stack10,dictionary10,list10,fileTable10,heap10,stmt10,1);
        IRepo repo10=new Repo("example.out.txt");
        repo10.add(prgState10);
        Controller ctrl10=new Controller(repo10);

        //Ex11: rH(a)
        IStmt stmt11=new PrintStmt(new rHExp("a"));
        MyIStack stack11=new MyStack<IStmt>();
        MyIDictionary<String,Integer> dictionary11=new MyDictionary<>();
        MyIList list11=new MyList<Integer>();
        MyIFileTable fileTable11=new MyFileTable();
        MyIHeap heap11=new MyHeap();

        PrgState prgState11=new PrgState(stack11,dictionary11,list11,fileTable11,heap11,stmt11,1);
        IRepo repo11=new Repo("example.out.txt");
        repo11.add(prgState11);
        Controller ctrl11=new Controller(repo11);

        //Ex12: wH(a)
        IStmt stmt12=new wHStmt("a",new ConstExp(12));
        MyIStack stack12=new MyStack<IStmt>();
        MyIDictionary<String,Integer> dictionary12=new MyDictionary<>();
        MyIList list12=new MyList<Integer>();
        MyIFileTable fileTable12=new MyFileTable();
        MyIHeap heap12=new MyHeap();

        PrgState prgState12=new PrgState(stack12,dictionary12,list12,fileTable12,heap12,stmt12,1);
        IRepo repo12=new Repo("example.out.txt");
        repo12.add(prgState12);
        Controller ctrl12=new Controller(repo12);

        //Ex13: 10+(2<6)
        IStmt stmt13=new PrintStmt(new ArithExp(new ConstExp(10),new BooleanExp(new ConstExp(2),new ConstExp(6),1),1));

        MyIStack stack13=new MyStack<IStmt>();
        MyIDictionary<String,Integer> dictionary13=new MyDictionary<>();
        MyIList list13=new MyList<Integer>();
        MyIFileTable fileTable13=new MyFileTable();
        MyIHeap heap13=new MyHeap();

        PrgState prgState13=new PrgState(stack13,dictionary13,list13,fileTable13,heap13,stmt13,1);
        IRepo repo13=new Repo("example.out.txt");
        repo13.add(prgState13);
        Controller ctrl13=new Controller(repo13);

        //Ex14: (10+2)<6
        IStmt stmt14=new PrintStmt(new BooleanExp(new ArithExp(new ConstExp(10),new ConstExp(2),1),new ConstExp(6),1));

        MyIStack stack14=new MyStack<IStmt>();
        MyIDictionary<String,Integer> dictionary14=new MyDictionary<>();
        MyIList list14=new MyList<Integer>();
        MyIFileTable fileTable14=new MyFileTable();
        MyIHeap heap14=new MyHeap();

        PrgState prgState14=new PrgState(stack14,dictionary14,list14,fileTable14,heap14,stmt14,1);
        IRepo repo14=new Repo("example.out.txt");
        repo14.add(prgState14);
        Controller ctrl14=new Controller(repo14);

        //Ex15: v=6;(while(v-4)print(v);v=v-1);print(v)
        AssignStmt a15=new AssignStmt("v",new ConstExp(6));
        PrintStmt p15=new PrintStmt(new VarExp("v"));
        AssignStmt a15_2=new AssignStmt("v",new ArithExp(new VarExp("v"),new ConstExp(1),2));
        WhileStmt w15=new WhileStmt(new ArithExp(new VarExp("v"),new ConstExp(4),2),new CmpStmt(p15,a15_2));
        //IStmt stmt15=new CmpStmt(new CmpStmt(a15,w15),p15);
        IStmt stmt15=new CmpStmt(a15,new CmpStmt(w15,p15));

        MyIStack stack15=new MyStack<IStmt>();
        MyIDictionary<String,Integer> dictionary15=new MyDictionary<>();
        MyIList list15=new MyList<Integer>();
        MyIFileTable fileTable15=new MyFileTable();
        MyIHeap heap15=new MyHeap();

        PrgState prgState15=new PrgState(stack15,dictionary15,list15,fileTable15,heap15,stmt15,1);
        IRepo repo15=new Repo("example.out.txt");
        repo15.add(prgState15);
        Controller ctrl15=new Controller(repo15);

        //Ex16: v=6;(while(v-4)print(a);v=v-1);print(v)
        AssignStmt a16=new AssignStmt("v",new ConstExp(6));
        CmpStmt c16=new CmpStmt(new PrintStmt(new VarExp("a")),new AssignStmt("v",new ArithExp(new VarExp("v"),new ConstExp(1),2)));
        WhileStmt w16=new WhileStmt(new ArithExp(new VarExp("v"),new ConstExp(4),2),c16);
        //IStmt stmt16=new CmpStmt(new CmpStmt(a16,w16),new PrintStmt(new VarExp("v")));
        IStmt stmt16=new CmpStmt(a16,new CmpStmt(w16,new PrintStmt(new VarExp("v"))));

        MyIStack stack16=new MyStack<IStmt>();
        MyIDictionary<String,Integer> dictionary16=new MyDictionary<>();
        MyIList list16=new MyList<Integer>();
        MyIFileTable fileTable16=new MyFileTable();
        MyIHeap heap16=new MyHeap();

        PrgState prgState16=new PrgState(stack16,dictionary16,list16,fileTable16,heap16,stmt16,1);
        IRepo repo16=new Repo("example.out.txt");
        repo16.add(prgState16);
        Controller ctrl16=new Controller(repo16);

        //Ex17:
        // openRFile(var_f17,"example.in.txt");
        // openRFile(var_ff17,"example.in.txt.txt");
        // readFile(var_f+2,var_c);print(var_c);
        // (if var_c then readFile(var_f,var_c);print(var_c)
        // else print(0));
        Exp var_f17=new VarExp("var_f17");
        Exp var_c17=new VarExp("var_c17");
        Exp var_ff17=new VarExp("var_ff17");
        openRFileStmt open17=new openRFileStmt("var_f17","example.in.txt");
        openRFileStmt open17_2=new openRFileStmt("var_ff17","emptyExample.in.txt.txt");
        readFileStmt read17=new readFileStmt(var_f17,"var_c17");
        IStmt readPrint17=new CmpStmt(read17,new PrintStmt(var_c17));
        IStmt if17=new IfStmt(var_c17,new CmpStmt(read17,new PrintStmt(var_c17)),new PrintStmt(new ConstExp(0)));
        closeRFileStmt close17=new closeRFileStmt(var_f17);
        //IStmt stmt17=new CmpStmt(new CmpStmt(new CmpStmt(open17,open17_2),readPrint17),new CmpStmt(if17,close17));
        IStmt stmt17=new CmpStmt(open17,new CmpStmt(open17_2,new CmpStmt(readPrint17,new CmpStmt(if17,close17))));

        MyIStack stack17=new MyStack<IStmt>();
        MyIDictionary<String,Integer> dictionary17=new MyDictionary<>();
        MyIList list17=new MyList<Integer>();
        MyIFileTable fileTable17=new MyFileTable();
        MyIHeap heap17=new MyHeap();

        PrgState prgState17=new PrgState(stack17,dictionary17,list17,fileTable17,heap17,stmt17,1);
        IRepo repo17=new Repo("example.out.txt");
        repo17.add(prgState17);
        Controller ctrl17=new Controller(repo17);

        //Ex18:v=10;new(a,22);fork(wH(a,30);v=32;print(v);print(rH(a)));print(v);print(rH(a))
        IStmt a18=new AssignStmt("v",new ConstExp(10));
        NewStmt n18=new NewStmt("a",new ConstExp(22));
        wHStmt w18=new wHStmt("a",new ConstExp(30));
        AssignStmt a18_2=new AssignStmt("v",new ConstExp(32));
        PrintStmt p18=new PrintStmt(new VarExp("v"));
        PrintStmt p18_2=new PrintStmt(new rHExp("a"));
        //IStmt fork=new CmpStmt(new CmpStmt(new CmpStmt(w18,a18_2),p18),p18_2);
        IStmt fork=new CmpStmt(w18,new CmpStmt(a18_2,new CmpStmt(p18,p18_2)));
        ForkStmt f18=new ForkStmt(fork);
        PrintStmt p18_3=new PrintStmt(new VarExp("v"));
        PrintStmt p18_4=new PrintStmt(new rHExp("a"));
        //IStmt stmt18=new CmpStmt(new CmpStmt(new CmpStmt(new CmpStmt(a18,n18),f18),p18_3),p18_4);
        IStmt stmt18=new CmpStmt(a18,new CmpStmt(n18,new CmpStmt(f18,new CmpStmt(p18_3,p18_4))));

        MyIStack stack18=new MyStack<IStmt>();
        MyIDictionary<String,Integer> dictionary18=new MyDictionary<>();
        MyIList list18=new MyList<Integer>();
        MyIFileTable fileTable18=new MyFileTable();
        MyIHeap heap18=new MyHeap();

        PrgState prgState18=new PrgState(stack18,dictionary18,list18,fileTable18,heap18,stmt18,1);
        IRepo repo18=new Repo("example.out.txt");
        repo18.add(prgState18);
        Controller ctrl18=new Controller(repo18);


        //Ex19:
        IStmt stmt19=new CmpStmt(new AssignStmt(("v"),new ConstExp(10)),new CmpStmt(new NewStmt("a",new ConstExp(22)),
                new CmpStmt(new ForkStmt(new CmpStmt(new wHStmt("a",new ConstExp(30)),new CmpStmt(new AssignStmt("v",new ConstExp(32)),
                        new CmpStmt(new PrintStmt(new VarExp("v")),new ForkStmt(new CmpStmt(new AssignStmt("t",new ConstExp(145)),
                                new PrintStmt(new VarExp("t")))))))),new CmpStmt(new PrintStmt(new VarExp("v")),new PrintStmt(new rHExp("a"))))));

        MyIStack stack19=new MyStack<IStmt>();
        MyIDictionary<String,Integer> dictionary19=new MyDictionary<>();
        MyIList list19=new MyList<Integer>();
        MyIFileTable fileTable19=new MyFileTable();
        MyIHeap heap19=new MyHeap();

        PrgState prgState19=new PrgState(stack19,dictionary19,list19,fileTable19,heap19,stmt19,1);
        IRepo repo19=new Repo("example.out.txt");
        repo19.add(prgState19);
        Controller ctrl19=new Controller(repo19);

        //Ex20:v=10;new(a,22);fork(wH(a,30);v=32;print(v);print(rH(a));fork(v=10;print(v)));print(v);print(rH(a))
        IStmt a20=new AssignStmt("v",new ConstExp(10));
        NewStmt n20=new NewStmt("a",new ConstExp(22));
        wHStmt w20=new wHStmt("a",new ConstExp(30));
        AssignStmt a20_2=new AssignStmt("v",new ConstExp(32));
        PrintStmt p20=new PrintStmt(new VarExp("v"));
        PrintStmt p20_2=new PrintStmt(new rHExp("a"));
        //IStmt fork=new CmpStmt(new CmpStmt(new CmpStmt(w18,a18_2),p18),p18_2);
        ForkStmt fork_2=new ForkStmt(new CmpStmt(new AssignStmt("v",new ConstExp(10)),new PrintStmt(new VarExp("v"))));
        //IStmt fork2=new CmpStmt(new AssignStmt("v",new ConstExp(10)),new PrintStmt(new VarExp("v")));
        //ForkStmt fork_2=new ForkStmt(fork2);
        IStmt fork20=new CmpStmt(w18,new CmpStmt(a18_2,new CmpStmt(p18,new CmpStmt(p18_2,fork_2))));
        ForkStmt f20=new ForkStmt(fork20);
        PrintStmt p20_3=new PrintStmt(new VarExp("v"));
        PrintStmt p20_4=new PrintStmt(new rHExp("a"));
        //IStmt stmt18=new CmpStmt(new CmpStmt(new CmpStmt(new CmpStmt(a18,n18),f18),p18_3),p18_4);
        IStmt stmt20=new CmpStmt(a20,new CmpStmt(n20,new CmpStmt(f20,new CmpStmt(p20_3,p20_4))));

        MyIStack stack20=new MyStack<IStmt>();
        MyIDictionary<String,Integer> dictionary20=new MyDictionary<>();
        MyIList list20=new MyList<Integer>();
        MyIFileTable fileTable20=new MyFileTable();
        MyIHeap heap20=new MyHeap();

        PrgState prgState20=new PrgState(stack20,dictionary20,list20,fileTable20,heap20,stmt20,1);
        IRepo repo20=new Repo("example.out.txt");
        repo20.add(prgState20);
        Controller ctrl20=new Controller(repo20);

        TextMenu menu=new TextMenu();
        menu.addCommand(new ExitCommand("0","Exit"));
        menu.addCommand(new RunExample("1",stmt1.toString(),ctrl1));
        menu.addCommand(new RunExample("2",stmt2.toString(),ctrl2));
        menu.addCommand(new RunExample("3",stmt3.toString(),ctrl3));
        menu.addCommand(new RunExample("4",stmt4.toString(),ctrl4));
        menu.addCommand(new RunExample("5",stmt5.toString(),ctrl5));
        menu.addCommand(new RunExample("6",stmt6.toString(),ctrl6));
        menu.addCommand(new RunExample("7",stmt7.toString(),ctrl7));
        menu.addCommand(new RunExample("8",stmt8.toString(),ctrl8));
        menu.addCommand(new RunExample("9",stmt9.toString(),ctrl9));
        menu.addCommand(new RunExample("10",stmt10.toString(),ctrl10));
        menu.addCommand(new RunExample("11",stmt11.toString(),ctrl11));
        menu.addCommand(new RunExample("12",stmt12.toString(),ctrl12));
        menu.addCommand(new RunExample("13",stmt13.toString(),ctrl13));
        menu.addCommand(new RunExample("14",stmt14.toString(),ctrl14));
        menu.addCommand(new RunExample("15",stmt15.toString(),ctrl15));
        menu.addCommand(new RunExample("16",stmt16.toString(),ctrl16));
        menu.addCommand(new RunExample("17",stmt17.toString(),ctrl17));
        menu.addCommand(new RunExample("18",stmt18.toString(),ctrl18));
        menu.addCommand(new RunExample("19",stmt19.toString(),ctrl19));
        menu.addCommand(new RunExample("20",stmt20.toString(),ctrl20));
        menu.show();

        //UI ui=new UI();
        //ui.mainMenu();
//        System.out.println("Ex1");
//        System.out.println(" v=2;Print(v)");
//        IStmt ex1=new CmpStmt(new AssignStmt("v",new ConstExp(2)),new PrintStmt(new VarExp("v")));
//
//        MyIStack stk=new MyStack<IStmt>();
//        MyIDictionary<String,Integer> symtbl=new MyDictionary<>();
//        MyIList ot=new MyList<Integer>();
//        PrgState prg=new PrgState(stk,symtbl,ot,ex1);
//
//        IRepo rep=new Repo();
//        rep.add(prg);
//        Controller ctrl=new Controller(rep);
//        try {
//            ctrl.allStep();
//        }catch (MyException exception){
//            System.out.println(exception.getMessage());
//        }
//
//        System.out.println("End of Ex1");
//        System.out.println();
//
//        System.out.println("Ex2");
//        System.out.println("a=2+3*5;b=a+1;Print(b)");
//        AssignStmt a=new AssignStmt("a",new ArithExp(new ConstExp(2),
//                new ArithExp(new ConstExp(3),new ConstExp(5),3),1));
//        AssignStmt b=new AssignStmt("b",new ArithExp(new VarExp("a"),new ConstExp(1),1));
//        PrintStmt p=new PrintStmt(new VarExp("b"));
//        IStmt ex2=new CmpStmt(new CmpStmt(a,b),p);
//
//        MyIStack stk2=new MyStack<IStmt>();
//        MyIDictionary<String,Integer> symtbl2=new MyDictionary<>();
//        MyIList ot2=new MyList<Integer>();
//        PrgState prg2=new PrgState(stk2,symtbl2,ot2,ex2);
//
//        IRepo rep2=new Repo();
//        rep2.add(prg2);
//        Controller ctrl2=new Controller(rep2);
//        try {
//            ctrl2.allStep();
//        }catch (MyException exception){
//            System.out.println(exception.getMessage());
//        }
//
//        System.out.println("End of Ex2");
//        System.out.println();
//
//        System.out.println("Ex3");
//        System.out.println("a=2-2;(If a Then v=2 Else v=3);Print(v)");
//        AssignStmt a3=new AssignStmt("a",new ArithExp(new ConstExp(2),new ConstExp(2),2));
//        IfStmt i=new IfStmt(new VarExp("a"),new AssignStmt("v",new ConstExp(2)),
//                new AssignStmt("v",new ConstExp(3)));
//        PrintStmt p3=new PrintStmt(new VarExp("v"));
//        IStmt ex3=new CmpStmt(new CmpStmt(a3,i),p3);
//
//        MyIStack stk3=new MyStack<IStmt>();
//        MyIDictionary<String,Integer> symtbl3=new MyDictionary<>();
//        MyIList ot3=new MyList<Integer>();
//        PrgState prg3=new PrgState(stk3,symtbl3,ot3,ex3);
//
//        IRepo rep3=new Repo();
//        rep3.add(prg3);
//        Controller ctrl3=new Controller(rep3);
//        try {
//            ctrl3.allStep();
//        }catch (MyException exception){
//            System.out.println(exception.getMessage());
//        }
//
//        System.out.print("End of Ex3");

//        System.out.println("Ex4");
//        System.out.println("a=2/0;print(a)");
//        AssignStmt a4=new AssignStmt("a",new ArithExp(new ConstExp(2),new ConstExp(0),4));
//
//        PrintStmt p4=new PrintStmt(new VarExp("a"));
//        IStmt ex4=new CmpStmt(a4,p4);
//
//        MyIStack stk4=new MyStack<IStmt>();
//        MyIDictionary<String,Integer> symtbl4=new MyDictionary<>();
//        MyIList ot4=new MyList<Integer>();
//        PrgState prg4=new PrgState(stk4,symtbl4,ot4,ex4);
//
//        IRepo rep4=new Repo();
//        rep4.add(prg4);
//        Controller ctrl4=new Controller(rep4);
//        try {
//            ctrl4.allStep();
//        }catch (MyException exception){
//            System.out.println(exception.getMessage());
//        }
//
//        System.out.print("End of Ex4");
    }
}
