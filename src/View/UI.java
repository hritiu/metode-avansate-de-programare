//package View;
//
//import Controller.Controller;
//import Model.*;
//import Repository.IRepo;
//import Repository.Repo;
//
//import java.lang.reflect.Array;
//import java.util.Arrays;
//import java.util.Scanner;
//import java.util.stream.IntStream;
//
//public class UI {
//
//    public UI(){}
//
//    public void printMenu(){
//        System.out.println("Choose a program:");
//        System.out.println("1.v=2;Print(v)");
//        System.out.println("2.a=2+3*5;b=a+1;Print(b)");
//        System.out.println("3.a=2-2;(If a Then v=2 Else v=3);Print(v)");
//        System.out.println("4.a=2/0;print(a)");
//        System.out.println("0.Exit.");
//    }
//
//    public int readCommand(String[] commands){
//        Scanner read=new Scanner(System.in);
//        boolean ok=false;
//        String op="";
//
//        while(!ok) {
//            System.out.print("Command: ");
//            op = read.next();
//            ok = Arrays.asList(commands).contains(op);
//        }
//
//        int cmd=Integer.parseInt(op);
//        return cmd;
//    }
//
//    void executeExample1(){
//        System.out.println("Ex1");
//        System.out.println(" v=2;Print(v)");
//        IStmt ex1=new CmpStmt(new AssignStmt("v",new ConstExp(2)),new PrintStmt(new VarExp("v")));
//
//        MyIStack stk=new MyStack<IStmt>();
//        MyIDictionary<String,Integer> symtbl=new MyDictionary<>();
//        MyIList ot=new MyList<Integer>();
//        //PrgState prg=new PrgState(stk,symtbl,ot,ex1);
//
//        IRepo rep=new Repo();
//        //rep.add(prg);
//        Controller ctrl=new Controller(rep);
//        try {
//            ctrl.allStep();
//        }catch (MyException exception){
//            System.out.println(exception.getMessage());
//        }
//
//        System.out.println("End of Ex1");
//        System.out.println();
//    }
//
//    void executeExample2(){
//        System.out.println("Ex2");
//        System.out.println("a=2+3*5;b=a+1;Print(b)");
//        AssignStmt a=new AssignStmt("a",new ArithExp(new ConstExp(2),
//                new ArithExp(new ConstExp(3),new ConstExp(5),3),1));
//        AssignStmt b=new AssignStmt("b",new ArithExp(new VarExp("a"),new ConstExp(1),1));
//        PrintStmt p=new PrintStmt(new VarExp("b"));
//        IStmt ex2=new CmpStmt(new CmpStmt(a,b),p);
//
//        MyIStack stk2=new MyStack<IStmt>();
//        MyIDictionary<String,Integer> symtbl2=new MyDictionary<>();
//        MyIList ot2=new MyList<Integer>();
//        //PrgState prg2=new PrgState(stk2,symtbl2,ot2,ex2);
//
//        IRepo rep2=new Repo();
//        //rep2.add(prg2);
//        Controller ctrl2=new Controller(rep2);
//        try {
//            ctrl2.allStep();
//        }catch (MyException exception){
//            System.out.println(exception.getMessage());
//        }
//
//        System.out.println("End of Ex2");
//        System.out.println();
//    }
//
//    void executeExample3(){
//        System.out.println("Ex3");
//        System.out.println("a=2-2;(If a Then v=2 Else v=3);Print(v)");
//        AssignStmt a3=new AssignStmt("a",new ArithExp(new ConstExp(2),new ConstExp(2),2));
//        IfStmt i=new IfStmt(new VarExp("a"),new AssignStmt("v",new ConstExp(2)),
//                new AssignStmt("v",new ConstExp(3)));
//        PrintStmt p3=new PrintStmt(new VarExp("v"));
//        IStmt ex3=new CmpStmt(new CmpStmt(a3,i),p3);
//
//        MyIStack stk3=new MyStack<IStmt>();
//        MyIDictionary<String,Integer> symtbl3=new MyDictionary<>();
//        MyIList ot3=new MyList<Integer>();
//        //PrgState prg3=new PrgState(stk3,symtbl3,ot3,ex3);
//
//        IRepo rep3=new Repo();
//        //rep3.add(prg3);
//        Controller ctrl3=new Controller(rep3);
//        try {
//            ctrl3.allStep();
//        }catch (MyException exception){
//            System.out.println(exception.getMessage());
//        }
//
//        System.out.print("End of Ex3");
//        System.out.println();
//    }
//
//    void executeExample4(){
//        System.out.println("Ex4");
//        System.out.println("a=2/0;print(a)");
//        AssignStmt a4=new AssignStmt("a",new ArithExp(new ConstExp(2),new ConstExp(0),4));
//
//        PrintStmt p4=new PrintStmt(new VarExp("a"));
//        IStmt ex4=new CmpStmt(a4,p4);
//
//        MyIStack stk4=new MyStack<IStmt>();
//        MyIDictionary<String,Integer> symtbl4=new MyDictionary<>();
//        MyIList ot4=new MyList<Integer>();
//        //PrgState prg4=new PrgState(stk4,symtbl4,ot4,ex4);
//
//        IRepo rep4=new Repo();
//        //rep4.add(prg4);
//        Controller ctrl4=new Controller(rep4);
//        try {
//            ctrl4.allStep();
//        }catch (MyException exception){
//            System.out.println(exception.getMessage());
//        }
//
//        System.out.print("End of Ex4");
//        System.out.println();
//    }
//
//    void mainMenu(){
//        boolean stop=false;
//        int cmd;
//        String commands[]={"0","1","2","3","4"};
//        while(!stop){
//            printMenu();
//            cmd=readCommand(commands);
//
//            if(cmd==0)
//                stop=true;
//            else if(cmd==1)
//                executeExample1();
//            else if(cmd==2)
//                executeExample2();
//            else if(cmd==3)
//                executeExample3();
//            else
//                executeExample4();
//        }
//    }
//
//
//    public void addNb(int a,int b,int sum){sum=a+b;}
//    public void addNb2(int a,int b, Integer sum){sum=a+b;}
//
//}
