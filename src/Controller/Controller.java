package Controller;

import Model.*;
import Repository.IRepo;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.BufferOverflowException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class Controller {
    private IRepo rep;
    private ExecutorService executor;

    public Controller(IRepo _rep){this.rep=_rep;}

    public Map<Integer,Integer> conservativeGarbageCollector(Collection<Integer> symTableValues,
                                                             Map<Integer,Integer> heap){
        Map<Integer,Integer> map=heap.entrySet().stream()
                .filter(e->symTableValues.contains(e.getKey()))
                .collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));
        HashMap<Integer,Integer> hashMap=new HashMap<>();
        for(Integer i:map.keySet())
            hashMap.put(i,map.get(i));
        return hashMap;
    }

    public HashMap<Integer,MyTuple<String,BufferedReader>> closeFiles(Collection<Integer> ids,HashMap<Integer,MyTuple<String,BufferedReader>> files){
        Map<Integer,MyTuple<String,BufferedReader>> map=files.entrySet().stream()
                .filter(e->ids.contains(e.getKey()))
                .collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));

        BufferedReader br=null;
        for(Integer i:map.keySet()){
            br=map.get(i).getB();
            try {
                br.close();
            }catch (IOException ioe){
                System.out.println("ERROR: I/O Exception. Cannot close file!!!");
            }
        }

        return new HashMap<>();
    }

    public void closeAllFiles(Collection<Integer> ids,HashMap<Integer,MyTuple<String,BufferedReader>> files){
        Map<Integer,MyTuple<String,BufferedReader>> map=files.entrySet().stream()
                .filter(e->ids.contains(e.getKey()))
                .collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));

        BufferedReader br=null;
        for(Integer i:map.keySet()){
            br=map.get(i).getB();
            try {
                br.close();
            }catch (IOException ioe){
                System.out.println("ERROR: I/O Exception. Cannot close file!!!");
            }
        }

        files.clear();
    }

    public List<PrgState> removeCompletedPrg(List<PrgState> inPrgList){
        return inPrgList.stream()
                .filter(p->p.isNotCompleted())
                .collect(Collectors.toList());
    }

    void oneStepForAllPrg(List<PrgState> prgList) throws InterruptedException{
        prgList.forEach(prg->rep.logPrgStateExec(prg));

        rep.getPrgList().forEach(p->System.out.println("Id: "+p.getId()+ "\n"+"Execution Stack: "+
                p.getExeStack().toString()+"\n"+"Symbol Table: "+p.getSymTable().toString()+"\n"+"Output: "
                +p.getOut().toString()+"\n"+ "File Table: "+p.getFileTable().toString()+"\n"+"Heap: "+
                p.getHeap().toString()+"\n"+"=>\n"));

        List<Callable<PrgState>> callList=prgList.stream()
                .map((PrgState p)->(Callable<PrgState>)(()->{return p.oneStep();}))
                .collect(Collectors.toList());

        List<PrgState> newPrgList=executor.invokeAll(callList).stream()
                .map(future->{try {
                    return future.get();
                }catch (InterruptedException|ExecutionException e){
                    System.out.println("ERROR: InterruptedException or ExecutionException !!!");
                }
                return null;
                })
                .filter(p->p!=null)
                .collect(Collectors.toList());

        prgList.addAll(newPrgList);

        prgList.forEach(prg->rep.logPrgStateExec(prg));

        rep.setPrgList(prgList);
    }

    public void allStep(){
        executor= Executors.newFixedThreadPool(2);

        List<PrgState> prgList=removeCompletedPrg(rep.getPrgList());

        while(prgList.size()>0){
            prgList.forEach(p->{
                        p.getHeap().setContent(conservativeGarbageCollector(p.getSymTable().getValues(),p.getHeap().getContent()));
                    });
            try{
                oneStepForAllPrg(prgList);
            }catch (InterruptedException ie){
                System.out.println("ERROR: InterruptedException !!!");
            }
            prgList=removeCompletedPrg(rep.getPrgList());
        }
        executor.shutdownNow();

        rep.getPrgList().forEach(p->p.getFileTable().getContent().forEach((descriptor,pair)->{
            try{
                pair.getB().close();
            }catch (IOException ioe){
                System.out.println("ERROR: IOException !!!");
            }
        }));
        rep.getPrgList().forEach(p->p.getFileTable().clearContent());

        rep.getPrgList().forEach(p->System.out.println("Id: "+p.getId()+ "\n"+"Execution Stack: "+
                p.getExeStack().toString()+"\n"+"Symbol Table: "+p.getSymTable().toString()+"\n"+"Output: "
                +p.getOut().toString()+"\n"+ "File Table: "+p.getFileTable().toString()+"\n"+"Heap: "+
                p.getHeap().toString()+"\n"+"=>\n"));

        rep.setPrgList(prgList);//after this => len=0
    }


//    public void oneStep(PrgState state){
//        MyIStack<IStmt> stk=state.getExeStack();
//        if(!stk.isEmpty()) {
//            IStmt crtStmt = stk.pop();
//            try {
//                crtStmt.execute(state);
//            }catch (MyException exception){
//                System.out.println(exception.getMessage());
//            }catch (IOException ioe){
//                System.out.println("ERROR: IOException !!! \n");
//            }
//
//            System.out.print("Execution Stack: ");
//            System.out.println(stk.toString());
//            System.out.print("Symbol Table: ");
//            System.out.println(state.getSymTable().toString());
//            System.out.print("Output: ");
//            System.out.println(state.getOut().toString());
//            System.out.print("File Table");
//            System.out.println(state.getFileTable().toString());
//            System.out.print("Heap");
//            System.out.println(state.getHeap().toString());
//            System.out.println("==>");
//        }
//    }

//    public void allStep() throws MyException {
//
//        try{
//            while(!prg.getExeStack().isEmpty()){
//                System.out.println();
//                oneStep(prg);
//
//                System.out.print("Execution Stack: ");
//                System.out.println(stk.toString());
//                System.out.print("Symbol Table: ");
//                System.out.println(state.getSymTable().toString());
//                System.out.print("Output: ");
//                System.out.println(state.getOut().toString());
//                System.out.print("File Table");
//                System.out.println(state.getFileTable().toString());
//                System.out.print("Heap");
//                System.out.println(state.getHeap().toString());
//                System.out.println("==>");
//
//                prg.getHeap().setContent(conservativeGarbageCollector(
//                        //prg.getSymTable().getContent().values(),
//                        prg.getSymTable().getValues(),
//                        prg.getHeap().getContent()));
//
//                this.rep.logPrgStateExec();
//            }
//        }catch (IOException e){
//            System.out.println("ERROR: "+e+" !!!\n");
//        }finally {
//            //prg.getFileTable().closeAll();
//            prg.getFileTable().setContent(closeFiles(prg.getFileTable().getKeySet(),prg.getFileTable().getContent()));
//       }
//
//
//        System.out.print("Execution Stack: ");
//        System.out.println(prg.getExeStack().toString());
//        System.out.print("Symbol Table: ");
//        System.out.println(prg.getSymTable().toString());
//        System.out.print("Output: ");
//        System.out.println(prg.getOut().toString());
//        System.out.print("File Table");
//        System.out.println(prg.getFileTable().toString());
//        System.out.print("Heap");
//        System.out.println(prg.getHeap().toString());
//        System.out.println("==>");
//    }
}